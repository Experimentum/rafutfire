package com.example.silver.rafutfirebase;

/**
 * Created by silver on 22.02.18.
 */

public class Users {

    public String name;
    public String image;
    public String thumb_image;
    public String status;
    public String uid;

    public Users(){}

    public Users(String name, String image, String status, String thumb_image, String uid) {
        this.name = name;
        this.image = image;
        this.status = status;
        this.thumb_image = thumb_image;
        this.uid = uid;
    }

    @Override
    public String toString() {
        return "Users{" +
                "uid='" + uid + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Users users = (Users) o;

        if (name != null ? !name.equals(users.name) : users.name != null) return false;
        if (image != null ? !image.equals(users.image) : users.image != null) return false;
        if (thumb_image != null ? !thumb_image.equals(users.thumb_image) : users.thumb_image != null)
            return false;
        if (status != null ? !status.equals(users.status) : users.status != null) return false;
        return uid != null ? uid.equals(users.uid) : users.uid == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (image != null ? image.hashCode() : 0);
        result = 31 * result + (thumb_image != null ? thumb_image.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (uid != null ? uid.hashCode() : 0);
        return result;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public void setName(String name) {this.name = name;}

    public void setImage(String image) {
        this.image = image;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {return name;}

    public String getImage() {
        return image;
    }

    public String getStatus() {
        return status;
    }

    public String getUid() {return uid;}

    public void setUid(String uid) {this.uid = uid;}
}
