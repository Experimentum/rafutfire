package com.example.silver.rafutfirebase;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Silver on 04.04.2018.
 */

public class CardVieHolder extends RecyclerView.ViewHolder {

    View mView;


    public CardVieHolder(View itemView) {
        super(itemView);
        mView = itemView;

    }
    public void setTimeAndDate(String time, String date){
        TextView post_timeAndDate = mView.findViewById(R.id.timeAndDate);
        String data = "Event starts at " +time+", on "+date;
        post_timeAndDate.setText(data);
    }


    public void setTitle(String title){
        TextView post_title = mView.findViewById(R.id.textTitle);
        post_title.setText(title);
    }

    public void setDesc(String desc){
        TextView post_desc = mView.findViewById(R.id.textDescription);
        System.out.println("this is desc"+ desc);
        post_desc.setText(desc);
    }

    public void setImage(Context ctx, String image){

        ImageView post_image = mView.findViewById(R.id.postImage);
        Picasso.with(ctx).load(image).into(post_image);
    }

    public void setUserName(String userName){
        TextView postUserName = mView.findViewById(R.id.textUsername);
        String text = "Started by: "+ userName;
        postUserName.setText(text);
    }
}
