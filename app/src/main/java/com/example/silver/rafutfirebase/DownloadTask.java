package com.example.silver.rafutfirebase;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by silver on 6.02.18.
 */

public class DownloadTask extends AsyncTask<String, String, String>  {

     static String TAG = "DownloadTask";

        @Override
        protected String doInBackground(String... url) {
            String data = "";

            try {
                data = EventActivity.downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.v(TAG, result);

            EventActivity.setMapData(result);

            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);

        }
    }



