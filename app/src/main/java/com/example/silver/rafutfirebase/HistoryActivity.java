package com.example.silver.rafutfirebase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HistoryActivity extends AppCompatActivity {

    private BarChart mChart;
    private FirebaseUser mCurrent_user;
    private RecyclerView longHistory;
    private DatabaseReference mActivityDatabase;
    ArrayList<RunData> distanceList;

    private final String TAG = "HistoryActivity";
    private Button view7;
    private Button view14;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        view7 = findViewById(R.id.view7);
        view14 = findViewById(R.id.view14);

        mChart = findViewById(R.id.chart1);
        mChart.getDescription().setEnabled(false);

        longHistory = (RecyclerView) findViewById(R.id.rv);

        longHistory.setFocusable(false);


        mCurrent_user = FirebaseAuth.getInstance().getCurrentUser();
        mActivityDatabase = FirebaseDatabase.getInstance().getReference().child("ActivityDatabase").child(mCurrent_user.getUid());

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        longHistory.setLayoutManager(mLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(longHistory.getContext(), mLayoutManager.getOrientation());
        longHistory.addItemDecoration(dividerItemDecoration);





        Query firebasesearchQuery = mActivityDatabase.orderByKey();


        FirebaseRecyclerAdapter<RunData, RunDataViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<RunData, RunDataViewHolder>(
                RunData.class,
                R.layout.history_data,
                RunDataViewHolder.class,
                firebasesearchQuery

        ) {
            @Override
            protected void populateViewHolder(RunDataViewHolder viewHolder, RunData model, int position) {

                viewHolder.setData(model);
            }
        };

        longHistory.setAdapter(firebaseRecyclerAdapter);



        distanceList = new ArrayList<>();

        mChart.setFitBars(true);

    }



    public void queryLast(int viewLast){

        mCurrent_user = FirebaseAuth.getInstance().getCurrentUser();
        mActivityDatabase = FirebaseDatabase.getInstance().getReference().child("ActivityDatabase").child(mCurrent_user.getUid());


        Query query = mActivityDatabase.orderByKey().limitToLast(viewLast);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){

                    System.out.println("snapshot.getKey() = " + snapshot.getKey());
                    distanceList.add(snapshot.getValue(RunData.class));

                }

                setData();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "Error : "+ databaseError);
            }

        });
    }




    public void onLast7Clicked(View view){
        if(distanceList.size() > 0){
            distanceList.clear();
        }
        queryLast(7);


    }

    public void onLast14Clicked(View view){

        if(distanceList.size() > 0){
            distanceList.clear();
        }
        queryLast(14);
    }

    private void setData(){

        int i = 0;

        ArrayList<BarEntry> yVals = new ArrayList<>();

        for (RunData elem : distanceList){

            i++;

            float value = Float.valueOf(elem.getDistance());
            yVals.add(new BarEntry(i, (int) value));

        }


        BarDataSet set = new BarDataSet(yVals, "");

        set.setColors(ColorTemplate.COLORFUL_COLORS);

        set.setDrawValues(true);

        BarData data = new BarData(set);


        mChart.setData(data);
        mChart.invalidate();
        mChart.animateY(500);
    }


}
