package com.example.silver.rafutfirebase;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainMenuActivity extends AppCompatActivity {
    private ActionBarDrawerToggle mToggle;
    private TextView header_name;
    private TextView header_loc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        DatabaseReference mDatabaseusers = FirebaseDatabase.getInstance().getReference().child("Users");

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_top);
        setSupportActionBar(mToolbar);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        final String user_id = mAuth.getCurrentUser().getUid();

        NavigationView navigationView = findViewById(R.id.nav_view);

        View navView = navigationView.inflateHeaderView(R.layout.navigation_header);

        CircleImageView imageView = navView.findViewById(R.id.profile_image_nav);

        header_name = navView.findViewById(R.id.header_username);
        header_loc = navView.findViewById(R.id.header_location);


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.nav_account) {
                    Intent intent = new Intent(MainMenuActivity.this, DataActivity.class);
                    startActivity(intent);
                    //finish();

                }
                if (item.getItemId() == R.id.nav_friends) {
                    Intent intent = new Intent(MainMenuActivity.this, FriendsListActivity.class);
                    startActivity(intent);
                    //finish();

                }

                if (item.getItemId() == R.id.nav_find_friends) {
                    Intent intent = new Intent(MainMenuActivity.this, AllUsersActivity.class);
                    startActivity(intent);
                    //finish();

                }

                if (item.getItemId() == R.id.nav_logout) {
                    Log.d("MainMenuActivity", "Atempting to log out");
                    FirebaseAuth.getInstance().signOut();
                    Intent intent = new Intent(MainMenuActivity.this, WelcomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }

                if (item.getItemId() == R.id.nav_password_change) {
                    Intent intent = new Intent(MainMenuActivity.this, PasswordActivity.class);
                    startActivity(intent);
                    //finish();

                }

                return false;
            }
        });


        DrawerLayout mDrawerLayout = findViewById(R.id.drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.closed);

        mDrawerLayout.addDrawerListener(mToggle);

        mToggle.syncState();


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mDatabaseusers.child(user_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //Getting the user data from the database
                String name = (String) dataSnapshot.child("name").getValue();
                String post_image = (String) dataSnapshot.child("thumb_image").getValue();
                String location = (String) dataSnapshot.child("location").getValue();


                header_name.setText(name);
                header_loc.setText(location);

                Picasso.with(getApplicationContext()).load(post_image).placeholder(R.drawable.profile).into(imageView);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Button viewHistory = (Button) findViewById(R.id.view_history_button);
        viewHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainMenuActivity.this, HistoryActivity.class);
                startActivity(intent);

            }
        });

        Button startTracking = (Button) findViewById(R.id.startTracking);
        startTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainMenuActivity.this, NonPermanentActivity.class); //RunActivity
                startActivity(intent);

            }
        });

        Button planEvent = (Button) findViewById(R.id.planEvent);
        planEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainMenuActivity.this, EventActivity.class);
                startActivity(intent);

            }
        });

        Button viewEvents = (Button) findViewById(R.id.viewEvents);
        viewEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainMenuActivity.this, TabActivity.class);
                startActivity(intent);

            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
