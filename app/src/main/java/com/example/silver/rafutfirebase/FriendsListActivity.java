package com.example.silver.rafutfirebase;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FriendsListActivity extends AppCompatActivity {

    private DatabaseReference mFriendsDatabase;
    private FirebaseUser mCurrent_user;
    private RecyclerView mListItem;
    private DatabaseReference mUsersDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_list);
        getSupportActionBar().hide();


        mFriendsDatabase = FirebaseDatabase.getInstance().getReference().child("Friends");
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mCurrent_user = FirebaseAuth.getInstance().getCurrentUser();

        Button friendRequestBtn = findViewById(R.id.view_friend_request_button);
        mListItem = findViewById(R.id.rv);
        mListItem.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mListItem.setLayoutManager(mLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mListItem.getContext(), mLayoutManager.getOrientation());
        mListItem.addItemDecoration(dividerItemDecoration);


        loadFriends();

        friendRequestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FriendsListActivity.this, FriendRequestActivity.class);
                startActivity(intent);
            }
        });


    }

    public void onBackButtonClicked(View view) {
        Intent intent = new Intent(FriendsListActivity.this, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void loadFriends() {

        final ArrayList<String> list_of_request = new ArrayList<>();


        // ------------------ Collecting together all the users who have sent us a friend request.---------------


        Query query = mFriendsDatabase.child(mCurrent_user.getUid());

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot requestSnapshot : dataSnapshot.getChildren()) {
                    String key = requestSnapshot.getKey();
                    list_of_request.add(key);
                }
                FriendRequestAdapter adapter = new FriendRequestAdapter(mUsersDatabase, list_of_request);
                mListItem.setAdapter(adapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


}
