package com.example.silver.rafutfirebase;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class DataActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private TextView username;
    private static String TAG = "DataActivity";
    private StorageReference mStorageref;
    private DatabaseReference mDatabaseusers;
    private Uri mImageUri = null;
    private static final int GALLERY_REQ =1;
    private static final int GALLERY =2;
    private EditText birthDateField;
    private EditText locationField;
    private EditText statusField;
    private CircleImageView circleImageView;
    private ProgressDialog progressDialog;
    private ImageView backgroundImage;
    private Uri returnBackgroungUri = null;
    private Uri returnImageUri = null;
    private String mBackgroundUriss = null;
    private String mImageUriss = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        View view = findViewById(R.id.data_scroll_view);
        view.setFocusable(false);

        Log.d(TAG, "DataActivity has been created");

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mDatabaseusers = FirebaseDatabase.getInstance().getReference().child("Users");
        mAuth = FirebaseAuth.getInstance();
        final String user_id = mAuth.getCurrentUser().getUid();
        username = findViewById(R.id.nameFieldInData);
        mStorageref = FirebaseStorage.getInstance().getReference().child("profile_image");
        birthDateField = findViewById(R.id.birthDate);
        locationField = findViewById(R.id.location);
        statusField = findViewById(R.id.status);
        circleImageView = findViewById(R.id.profile_image);
        progressDialog = new ProgressDialog(this);
        backgroundImage = findViewById(R.id.data_background_image);


        //GETTING AND SETTING USER DATA FIELDS
        mDatabase.child(user_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "on data changed data snapshot");

                //Getting the user data from the database
                String name = (String) dataSnapshot.child("name").getValue();
                String post_image = (String) dataSnapshot.child("thumb_image").getValue();
                String background = (String) dataSnapshot.child("background").getValue();
                String bithday = (String) dataSnapshot.child("birthDate").getValue();
                String location = (String) dataSnapshot.child("location").getValue();
                String status = (String) dataSnapshot.child("status").getValue();


                //Setting the user data to the appropriate fields.
                Picasso.with(DataActivity.this).load(post_image).placeholder(R.drawable.profile).into(circleImageView);
                Picasso.with(DataActivity.this).load(background).placeholder(R.drawable.backgroungimage).into(backgroundImage);

                username.setText(name);
                birthDateField.setText(bithday);
                locationField.setText(location);
                statusField.setText(status);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "We got a database error: " + databaseError);

            }
        });
    }


    public void onBackButtonClicked(View view){
        Intent intent = new Intent(DataActivity.this, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }




    public void dataChangeButtonClicked(View view){

        progressDialog.setMessage("Saving changes");
        progressDialog.show();

        final String user_id = mAuth.getCurrentUser().getUid();

        String location = locationField.getText().toString().trim();
        String birthDate = birthDateField.getText().toString().trim();
        String statusInfo = statusField.getText().toString().trim();

        mDatabaseusers.child(user_id).child("location").setValue(location);
        mDatabaseusers.child(user_id).child("birthDate").setValue(birthDate);
        mDatabaseusers.child(user_id).child("status").setValue(statusInfo);

        Log.d(TAG, "mImageUri : " +mImageUri);

        if(returnBackgroungUri != null && returnImageUri !=null){
            backgroundChange(user_id , false);
            imageChanged(user_id, true);

        }


        else if (returnBackgroungUri != null){
            backgroundChange(user_id, true);

        }

        else if(returnImageUri != null){

            imageChanged(user_id, true);
        }

        else{
            progressDialog.dismiss();

            Log.d(TAG, "Other intent");

            Intent intent = new Intent(DataActivity.this, MainMenuActivity.class);
            startActivity(intent);
            finish();


        }


    }


    public void backgroundChange(String user_id, boolean start_intent){
        Log.d(TAG, "Background has been changed");

        File filePath = new File(returnBackgroungUri.getPath());

        Bitmap background_file = null;

        try {
            Log.d(TAG, "FILEPATH: "+ filePath);
            background_file = new Compressor(this)
                    .setQuality(75)
                    .compressToBitmap(filePath);
        }catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        background_file.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        final byte[] background_byte = baos.toByteArray();

        final StorageReference background_image_path =mStorageref.child("background_image").child(user_id + ".jpg");

        StorageReference filepath = mStorageref.child(returnBackgroungUri.getLastPathSegment());

        filepath.putFile(returnBackgroungUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                final String downloadurl = String.valueOf(taskSnapshot.getDownloadUrl());

                UploadTask uploadTask = background_image_path.putBytes(background_byte);
                uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {


                        if(task.isSuccessful()){

                            Map<String, Object> update_hasmap = new HashMap<>();
                            update_hasmap.put("background", downloadurl);

                            mDatabaseusers.child(user_id).updateChildren(update_hasmap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {

                                    if(start_intent){
                                        progressDialog.dismiss();

                                        Log.d(TAG, "Intent after background was changed");

                                        Intent intent = new Intent(DataActivity.this, MainMenuActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }

                                    else{
                                        Log.d(TAG, "We did not start the intent");
                                    }



                                }
                            });

                        }

                    }
                });

            }
        });

    }


    public void imageChanged(String user_id, boolean start_intent){
        Log.d(TAG, "Profile has been changed");



        File filePath = new File(returnImageUri.getPath());
        Log.d(TAG, "FILEPATHhue: "+ filePath);


        Bitmap thumb_file = null;

        try {
            thumb_file = new Compressor(this) .setMaxWidth(200)
                    .setMaxHeight(200)
                    .setQuality(75)
                    .compressToBitmap(filePath);
        }catch (IOException e) {
            e.printStackTrace();
        }


        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        thumb_file.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        final byte[] thumb_byte = baos.toByteArray();

        final StorageReference thumb_file_path =mStorageref.child("profile_image").child("thumbs").child(user_id + ".jpg");

        StorageReference filepath = mStorageref.child(returnImageUri.getLastPathSegment());



        filepath.putFile(returnImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                final String downloadurl = String.valueOf(taskSnapshot.getDownloadUrl());

                UploadTask uploadTask = thumb_file_path.putBytes(thumb_byte);
                uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> thumb_task) {

                        String thumb_downloadURL = thumb_task.getResult().getDownloadUrl().toString();

                        if(thumb_task.isSuccessful()){

                            Map<String, Object> update_hasmap = new HashMap<>();
                            update_hasmap.put("image", downloadurl);
                            update_hasmap.put("thumb_image", thumb_downloadURL);

                            mDatabaseusers.child(user_id).updateChildren(update_hasmap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {

                                    if(start_intent){


                                        Log.d(TAG, "Intent after profile was changed");


                                        progressDialog.dismiss();

                                        Intent intent = new Intent(DataActivity.this, MainMenuActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                    else{
                                        Log.d(TAG, "We didn not start the intent");
                                    }


                                }
                            });

                        }
                    }
                });

            }
        });
    }



    public void onBackgroundImageClicked(View view){
        Log.d(TAG, "change backgroung button has been clicked");
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        this.startActivityForResult(galleryIntent, GALLERY);
    }




    public void changeImageButtonClicked(View view){
        Log.d(TAG, "change image button has been clicked");
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        this.startActivityForResult(galleryIntent, GALLERY_REQ);
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY && resultCode == RESULT_OK){

            Uri imageUri = data.getData();
            mBackgroundUriss = "not null";
            CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(16,9)
                    .start(this);

        }


        if(requestCode == GALLERY_REQ && resultCode == RESULT_OK){

            returnImageUri = data.getData();
            mImageUriss = "not null";
            CropImage.activity(returnImageUri).setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1,1)
                    .start(this);
        }

        if(mBackgroundUriss != null) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri mBackgroundUri = null;
                if (resultCode == RESULT_OK) {
                    mBackgroundUri = result.getUri();
                    returnBackgroungUri = result.getUri();
                    backgroundImage.setImageURI(mBackgroundUri);
                    mBackgroundUriss = null;

                } else if (resultCode == RESULT_CANCELED) {
                    Log.d(TAG, "Result has been cancelled");
                    mBackgroundUriss = null;

                }

            }
        }


        if(mImageUriss != null){

        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if(resultCode == RESULT_OK){
                mImageUri = result.getUri();
                returnImageUri = result.getUri();
                circleImageView.setImageURI(mImageUri);
                mImageUri = null;
                mImageUriss = null;

            }

            else if (resultCode == RESULT_CANCELED){
                Log.d(TAG, "Result has been cancelled");
                mImageUri = null;
                mImageUriss = null;

            }
        }
        else if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
            Log.d(TAG, "Something has gone wrong with the activity result part");
        }
        }



    }

    //For looking at the life cycle of the application
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "DataActivity has been stopped");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "DataActivity has been destroyed");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "DataActivity has been resumed");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "DataActivity has been restarted");

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "DataActivity has been started");
    }
}
