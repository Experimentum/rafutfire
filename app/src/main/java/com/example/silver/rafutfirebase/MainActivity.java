package com.example.silver.rafutfirebase;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mListItem;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private PublicPostsAdapter mAdapter;
    private static final String TAG = "MainActivity";
    private int mTotalItemCount = 0;
    private int mLastVisibleItemPosition;
    private int mPostsPerPage = 5;



    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("rafutfirebase");
        mAuth = FirebaseAuth.getInstance();
        mListItem = findViewById(R.id.list_item);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mListItem.setLayoutManager(mLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mListItem.getContext(),
                mLayoutManager.getOrientation());
        mListItem.addItemDecoration(dividerItemDecoration);

        mAdapter = new PublicPostsAdapter();
        mListItem.setAdapter(mAdapter);


        getPosts(-1);

        mListItem.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        System.out.println("The RecyclerView is not scrolling");
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        System.out.println("Scrolling now");
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        System.out.println("Scroll Settling");


                        mTotalItemCount = mLayoutManager.getItemCount();
                        mLastVisibleItemPosition = mLayoutManager.findLastVisibleItemPosition();

                        Log.d(TAG, "mLastVisibleItemPosition " + mLastVisibleItemPosition);
                        Log.d(TAG, "mPostsPerPage "+ mPostsPerPage);

                        int value = mLastVisibleItemPosition + mPostsPerPage;



                        Log.d(TAG, "totalitemcount : "+ mTotalItemCount + " mLastVisibleItemPosition + mPostsPerPage "+ value);

                        if (mTotalItemCount <= (mLastVisibleItemPosition + mPostsPerPage)) {
                            Log.d(TAG, "The id of the last visible post: " + mAdapter.getLastItemId());
                            getPosts(mAdapter.getLastItemId());
                        }
                        else{
                            mListItem.clearOnScrollListeners();
                        }

                }
            }
        });



        // NO IDEA WHAT THIS CODE DOES ANYMORE, CANT REMEMBER
        //-----------------------------------------
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null){
                    Intent loginIntent = new Intent(MainActivity.this, WelcomeActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(loginIntent);
                    finish();

                }
            }
        };
        //-----------------------------------------


    }

    public void getPosts(long nodeId){

        Query query;

        Query temp = mDatabase.orderByChild("creation_date");


        Log.d(TAG, "This is our node value : " + nodeId);



        if(nodeId == -1){
            System.out.println("inital round");
            query = temp.limitToFirst(mPostsPerPage);
        }
        else{
            System.out.println("we are going for another round");

            query = temp.startAt(nodeId); // .limitToFirst(mPostsPerPage);
            query = query.limitToFirst(mPostsPerPage);

        }

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println("started query");
                List<Post> postList = new ArrayList<>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    postList.add(postSnapshot.getValue(Post.class));
                    System.out.println("post : " + postSnapshot.getValue(Post.class).getPost_id());
                }

                mAdapter.addAll(postList);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "MainActivity has been destroyed");
        mAuth.removeAuthStateListener(mAuthListener);
        mListItem.setAdapter(null);
        mListItem.addOnScrollListener(null);
    }

}
