package com.example.silver.rafutfirebase;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FriendRequestActivity extends AppCompatActivity {

    private DatabaseReference mFriendReqDatabase;
    private FirebaseUser mCurrent_user;
    private RecyclerView mUsersList;
    private String TAG = "FriendRequestActivity";
    private DatabaseReference mUsersDatabase;
    List<Users> usersList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_request);
        getSupportActionBar().hide();


        mFriendReqDatabase = FirebaseDatabase.getInstance().getReference().child("Friend_req");
        mCurrent_user = FirebaseAuth.getInstance().getCurrentUser();
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        usersList = new ArrayList<>();


        mUsersList = (RecyclerView) findViewById(R.id.rv);

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mUsersList.setLayoutManager(mLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mUsersList.getContext(), mLayoutManager.getOrientation());
        mUsersList.addItemDecoration(dividerItemDecoration);


        try {
            loadRequests();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    public void onBackButtonClicked(View view) {
        Intent intent = new Intent(FriendRequestActivity.this, FriendsListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }


    public void loadRequests() throws InterruptedException {

        final ArrayList<String> list_of_request = new ArrayList<>();


        // ------------------ Collecting together all the users who have sent us a friend request.---------------


        Query query = mFriendReqDatabase.child(mCurrent_user.getUid()).orderByChild("request_type").equalTo("received");
        //mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users");


        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot requestSnapshot : dataSnapshot.getChildren()) {
                    String key = requestSnapshot.getKey();
                    list_of_request.add(key);
                }
                FriendRequestAdapter adapter = new FriendRequestAdapter(mUsersDatabase, list_of_request);
                mUsersList.setAdapter(adapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //-------------------------------------------------------------------------------------------------------
    }


}

