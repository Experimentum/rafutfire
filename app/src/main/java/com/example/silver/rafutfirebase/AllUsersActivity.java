package com.example.silver.rafutfirebase;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AllUsersActivity extends AppCompatActivity {

    private static String TAG = "AllUsersActivity";
    private RecyclerView mUsersList;
    private UserRVAdapter mAdapter;
    private int mTotalItemCount = 0;
    private int mPostsPerPage = 6;
    private int mLastVisibleItemPosition;
    private EditText search_field;
    private ImageButton search_button;


    private DatabaseReference mUsersDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_users);


        // search field setup
        search_field = findViewById(R.id.search_edit_text_test);
        search_button = findViewById(R.id.search_button);


        search_field.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            //NOT THE MOST IDEAL SOLUTION
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 1) {
                    if (s.charAt(s.length() - 1) == '\n') {
                        String value = s.toString();
                        String searchText = value.substring(0, s.length() - 1);
                        search_field.setText(searchText);
                        firebaseUserSearch(searchText);
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mUsersList = (RecyclerView) findViewById(R.id.rv);

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mUsersList.setLayoutManager(mLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mUsersList.getContext(), mLayoutManager.getOrientation());
        mUsersList.addItemDecoration(dividerItemDecoration);

        mAdapter = new UserRVAdapter();

        mUsersList.setAdapter(mAdapter);

        getUsers(null);


        mUsersList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        System.out.println("The RecyclerView is not scrolling");
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        System.out.println("Scrolling now");
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        System.out.println("Scroll Settling");

                        mTotalItemCount = mLayoutManager.getItemCount();
                        mLastVisibleItemPosition = mLayoutManager.findLastVisibleItemPosition();//findLastVisibleItemPosition
                        //mLastVisibleItemPosition = mAdapter.getItemCount();

                        Log.d(TAG, "mLastVisibleItemPosition " + mLastVisibleItemPosition);
                        Log.d(TAG, "mPostsPerPage " + mPostsPerPage);

                        int value = mLastVisibleItemPosition + mPostsPerPage;


                        Log.d(TAG, "totalitemcount : " + mTotalItemCount + " mLastVisibleItemPosition + mPostsPerPage " + value);

                        if (mTotalItemCount <= (mLastVisibleItemPosition + mPostsPerPage)) {
                            Log.d(TAG, "Loading in a new batch of users");
                            getUsers(mAdapter.getLastItemId());
                        } else {
                            mUsersList.clearOnScrollListeners();
                        }
                        //break;

                }
            }
        });


        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String searchText = search_field.getText().toString();

                firebaseUserSearch(searchText);
            }
        });

    }

    private void firebaseUserSearch(String searchText) {

        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");

        Query firebasesearchQuery = mUsersDatabase.orderByChild("name").startAt(searchText).endAt(searchText + "\uf8ff");


        FirebaseRecyclerAdapter<Users, UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Users, UsersViewHolder>(
                Users.class,
                R.layout.users_single_layout,
                UsersViewHolder.class,
                firebasesearchQuery


        ) {
            @Override
            protected void populateViewHolder(UsersViewHolder viewHolder, Users model, int position) {
                viewHolder.setData(model);

                final String user_id = getRef(position).getKey();

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent profile_intent = new Intent(AllUsersActivity.this, ProfileActivity.class);
                        profile_intent.putExtra("user_id", user_id);
                        startActivity(profile_intent);
                        finish();
                    }
                });
            }

        };

        mUsersList.setAdapter(firebaseRecyclerAdapter);

    }


    private void getUsers(String nodeId) {


        Log.d(TAG, "This is our node value : " + nodeId);

        Query query;
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");

        if (nodeId == null) {

            query = mUsersDatabase.orderByKey().limitToFirst(mPostsPerPage);
        } else {
            query = mUsersDatabase.orderByKey().startAt(nodeId).limitToFirst(mPostsPerPage);

        }

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Users user;
                List<Users> usersList = new ArrayList<>();

                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    usersList.add(userSnapshot.getValue(Users.class));
                }


                mAdapter.addAll(usersList);
                Toast.makeText(getApplicationContext(), "Loaded in more users", Toast.LENGTH_LONG).show();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }
}





