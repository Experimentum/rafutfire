package com.example.silver.rafutfirebase;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.support.design.widget.TabLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TableLayout;

public class TabActivity extends android.app.TabActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        TabHost tabHost = getTabHost();
        TabHost.TabSpec spec;
        Intent intent;

        intent = new Intent().setClass(this, MainActivity.class);
        spec = tabHost.newTabSpec("Public").setIndicator("Public")
                .setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, FriendsViewActivity.class);
        spec = tabHost.newTabSpec("Friends").setIndicator("Friends")
                .setContent(intent);
        tabHost.addTab(spec);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}