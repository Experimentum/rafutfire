package com.example.silver.rafutfirebase;

import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by Silver on 02.05.2018.
 */


public class FirebaseNotificationService extends FirebaseMessagingService {

    public static String TAG = "FirebaseNotificationService";

    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String notficationTitle = remoteMessage.getData().get("title");
        String notficationMessage = remoteMessage.getData().get("body");
        String click_action = remoteMessage.getData().get("click_action");
        String from_user_id = remoteMessage.getData().get("from_user_id");


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "CHANNEL_ID")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(notficationTitle)
                .setContentText(notficationMessage)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);


        if (user != null) {

            Log.d(TAG, "User is authenticated");

            Intent resultIntent = new Intent(click_action);
            Log.d(TAG, "user_id: " + from_user_id);
            resultIntent.putExtra("user_id", from_user_id);

            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);


            mBuilder.setContentIntent(resultPendingIntent);

            int mNotificated = (int) System.currentTimeMillis();
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            notificationManager.notify(mNotificated, mBuilder.build());
        } else {
            Log.d(TAG, "User is not authenticated");

            int mNotificated = (int) System.currentTimeMillis();
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            notificationManager.notify(mNotificated, mBuilder.build());

        }

    }
}
