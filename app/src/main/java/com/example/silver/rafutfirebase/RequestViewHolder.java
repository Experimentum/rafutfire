package com.example.silver.rafutfirebase;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Silver on 03.04.2018.
 */

public class RequestViewHolder extends RecyclerView.ViewHolder {

    TextView userNameView;
    CircleImageView userImageView;

    View mView;


    public RequestViewHolder(View itemView) {
        super(itemView);
        findViews(itemView);
        mView = itemView;
    }

    public void findViews(View view){
        userNameView = view.findViewById(R.id.request_single_name);
        userImageView = view.findViewById(R.id.request_single_image);
    }

    public void setData(Request request){
        Log.d("HUEHUE", String.valueOf(request));
        userNameView.setText(request.getName());
        Picasso.with(userImageView.getContext()).load(request.thumb_image).placeholder(R.drawable.profile).into(userImageView);
    }

}
