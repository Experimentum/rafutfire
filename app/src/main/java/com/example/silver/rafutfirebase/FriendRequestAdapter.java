package com.example.silver.rafutfirebase;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by Silver on 04.04.2018.
 */

public class FriendRequestAdapter extends RecyclerView.Adapter<UsersViewHolder> {


    String key = null;
    ArrayList<Users> items = new ArrayList<>();


    public FriendRequestAdapter(DatabaseReference ref, final ArrayList<String> list_of_requests){

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                        key = postSnapshot.getKey();

                        Users user= postSnapshot.getValue(Users.class);

                        if(list_of_requests.contains(user.getUid())){

                            items.add(user);

                        }


                    }
                }
                notifyDataSetChanged();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    @NonNull
    @Override
    public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.users_single_layout, parent, false);


        return new UsersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UsersViewHolder holder, int position) {
        Users item = items.get(position);
        final String user_id = item.getUid();
        holder.setData(item);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profile_intent = new Intent(holder.mView.getContext(), ProfileActivity.class);
                profile_intent.putExtra("user_id", user_id);
                holder.mView.getContext().startActivity(profile_intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
