package com.example.silver.rafutfirebase;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Silver on 28.03.2018.
 */

public class UsersViewHolder extends RecyclerView.ViewHolder {

    TextView userNameView;
    TextView statusView;
    CircleImageView userImageView;

    View mView;

    public UsersViewHolder(View itemView) {
        super(itemView);
        findViews(itemView);
        mView = itemView;
    }

    public void findViews(View view){
        userNameView = view.findViewById(R.id.user_single_name);
        statusView = view.findViewById(R.id.user_single_status);
        userImageView = view.findViewById(R.id.user_single_image);
    }

    public void setData(Users user){
      userNameView.setText(user.getName());
      statusView.setText(user.getStatus());
      Picasso.with(userImageView.getContext()).load(user.thumb_image).placeholder(R.drawable.profile).into(userImageView);


    }

}