package com.example.silver.rafutfirebase;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

public class ProfileActivity extends AppCompatActivity {

    private TextView mProfileName;
    private TextView mProfileStatus;
    private TextView mProfileFriendsCount;
    private ImageView mProfileImage;
    private ImageView mBackgroundImage;
    private Button mProfileDecline;
    private Button mProfileSendReqBtn;

    private DatabaseReference mUsersDatabase;

    private ProgressDialog mProgressDialog;
    private FirebaseUser mCurrent_user;
    private DatabaseReference mFriendReqDatabase;
    private DatabaseReference mFriendDatabase;
    private DatabaseReference mNotificationDatabase;
    private String TAG = "ProfileActivity";

    private int mCurrent_State;
    private final int not_friends = -1;
    private final int request_sent = 0;
    private final int friends = 2;
    private final int request_received = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getSupportActionBar().hide();


        final String user_id = getIntent().getStringExtra("user_id");

        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_id);
        mFriendReqDatabase = FirebaseDatabase.getInstance().getReference().child("Friend_req");
        mFriendDatabase = FirebaseDatabase.getInstance().getReference().child("Friends");
        mNotificationDatabase = FirebaseDatabase.getInstance().getReference().child("notifications");


        mProfileImage = findViewById(R.id.profile_image);
        mBackgroundImage = findViewById(R.id.profile_background_image);
        mProfileName = findViewById(R.id.profile_name);
        mProfileStatus = findViewById(R.id.profile_staus);
        mProfileFriendsCount = findViewById(R.id.profile_friend_count);
        mProfileSendReqBtn = findViewById(R.id.profile_send_req);
        mProfileDecline = findViewById(R.id.decline_button);


        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait, loading user data");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        mCurrent_State = -1; // state -1 is not friends
        mCurrent_user = FirebaseAuth.getInstance().getCurrentUser();

        mProfileDecline.setVisibility(View.INVISIBLE);
        mProfileDecline.setEnabled(false);

        mUsersDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {




                String display_name = dataSnapshot.child("name").getValue().toString();
                String status = dataSnapshot.child("status").getValue().toString();
                String image = dataSnapshot.child("image").getValue().toString();
                Object background = dataSnapshot.child("background").getValue();


                mProfileName.setText(display_name);
                mProfileStatus.setText(status);

                Picasso.with(ProfileActivity.this).load(image).placeholder(R.drawable.profile).into(mProfileImage);
                if(background != null){
                    Picasso.with(ProfileActivity.this).load(background.toString()).placeholder(R.drawable.backgroungimage).into(mBackgroundImage);
                }


                // ----------  Reading in user data ---------- //

                mFriendReqDatabase.child(mCurrent_user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if(Objects.equals(mCurrent_user.getUid(), user_id)){
                            Log.d(TAG, "You are looking at yourself dumb dumb");
                            mProfileSendReqBtn.setEnabled(false);
                            mProfileSendReqBtn.setText("This is you");
                        }


                        if(dataSnapshot.hasChild(user_id)){
                            String req_type = dataSnapshot.child(user_id).child("request_type").getValue().toString();


                            // If I have received a friend request.

                            if (req_type.equals("received")){

                                mCurrent_State = request_received; // request received
                                mProfileSendReqBtn.setText(R.string.accept_friend_request);

                                mProfileDecline.setVisibility(View.VISIBLE);
                                mProfileDecline.setEnabled(true);

                            }

                            // If there is a friend request sent to me.

                            else if (req_type.equals("sent")){

                                mCurrent_State = request_sent;
                                mProfileSendReqBtn.setText(R.string.cancel_friend_request);

                                mProfileDecline.setVisibility(View.INVISIBLE);
                                mProfileDecline.setEnabled(false);
                            }
                            mProgressDialog.dismiss();

                        }

                        else{
                            mFriendDatabase.child(mCurrent_user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.hasChild(user_id)){

                                        mCurrent_State = friends; // friends
                                        mProfileSendReqBtn.setText(R.string.unfriend_user);

                                        mProfileDecline.setVisibility(View.INVISIBLE);
                                        mProfileDecline.setEnabled(false);
                                    }
                                    mProgressDialog.dismiss();

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                    mProgressDialog.dismiss();

                                }
                            });
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                // ---------- End of reading in user data ------------ //




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        mFriendDatabase.child(user_id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int friend_count = (int) dataSnapshot.getChildrenCount();
                String friends_count_string = "Friend count : "+String.valueOf(friend_count);
                mProfileFriendsCount.setText(friends_count_string);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        // ------- We have read in the profile data  ------------------ //




        Log.d(TAG, "Profile data has been sucessfully read from the database");

        // ------------------------------------------------------------ //


        // ------- We have cliecked on the friend request button------- //

        mProfileSendReqBtn.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                Log.d(TAG, "Send request button has been clicked");

                mProfileSendReqBtn.setEnabled(false);


                if(mCurrent_State == not_friends){
                    notFriends(user_id);

                }

                if(mCurrent_State == request_sent){
                    requestSent(user_id);

                }

                if(mCurrent_State == request_received){
                    requestReceived(user_id);

                }

                if(mCurrent_State == friends){

                    alreadyFriends(user_id);
                }



            } // End of onClick method
        }); // End of the onClick listener


        mProfileDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCurrent_State == request_received){

                    mFriendReqDatabase.child(mCurrent_user.getUid()).child(user_id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            mFriendReqDatabase.child(user_id).child(mCurrent_user.getUid()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {


                                    mProfileSendReqBtn.setEnabled(true);

                                    mCurrent_State = not_friends; // not friends
                                    mProfileSendReqBtn.setText(R.string.send_friend_request);

                                    mProfileDecline.setVisibility(View.INVISIBLE);
                                    mProfileDecline.setEnabled(false);


                                }
                            });
                        }
                    });


                }
            }
        });


    }

    //-----------------------------------------------------------------------

    public void alreadyFriends(String user_id){
        mFriendDatabase.child(mCurrent_user.getUid()).child(user_id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                mFriendDatabase.child(user_id).child(mCurrent_user.getUid()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {


                        mProfileSendReqBtn.setEnabled(true);

                        mCurrent_State = not_friends; // not friends
                        mProfileSendReqBtn.setText(R.string.send_friend_request);

                        mProfileDecline.setVisibility(View.INVISIBLE);
                        mProfileDecline.setEnabled(false);
                    }
                });
            }
        });
    }

    //-----------------------------------------------------------------------


    public void requestReceived(String user_id){
        final String currentDate = DateFormat.getDateTimeInstance().format(new Date());

        mFriendDatabase.child(mCurrent_user.getUid()).child(user_id).setValue(currentDate).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mFriendDatabase.child(user_id).child(mCurrent_user.getUid()).setValue(currentDate).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {


                        mFriendReqDatabase.child(mCurrent_user.getUid()).child(user_id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                mFriendReqDatabase.child(user_id).child(mCurrent_user.getUid()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {


                                        mProfileSendReqBtn.setEnabled(true);

                                        mCurrent_State = friends; // friends
                                        mProfileSendReqBtn.setText(R.string.unfriend_user);

                                        mProfileDecline.setVisibility(View.INVISIBLE);
                                        mProfileDecline.setEnabled(false);


                                    }
                                });
                            }
                        });

                    }
                });
            }
        });
    }

    //-----------------------------------------------------------------------

    public void requestSent(String user_id){
        mFriendReqDatabase.child(mCurrent_user.getUid()).child(user_id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                mFriendReqDatabase.child(user_id).child(mCurrent_user.getUid()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {


                        mProfileSendReqBtn.setEnabled(true);

                        mCurrent_State = not_friends; // not friends
                        mProfileSendReqBtn.setText(R.string.send_friend_request);

                        mProfileDecline.setVisibility(View.INVISIBLE);
                        mProfileDecline.setEnabled(false);


                    }
                });
            }
        });
    }

    //-----------------------------------------------------------------------

    public void notFriends(String user_id){
        mProfileDecline.setVisibility(View.INVISIBLE);
        mProfileDecline.setEnabled(false);

        mFriendReqDatabase.child(mCurrent_user.getUid()).child(user_id).child("request_type").setValue("sent").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if(task.isSuccessful()){

                    mFriendReqDatabase.child(user_id).child(mCurrent_user.getUid()).child("request_type").setValue("received").addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                            HashMap<String, String> notification_data = new HashMap<>();
                            notification_data.put("from", mCurrent_user.getUid());
                            notification_data.put("type", "request");


                            mNotificationDatabase.child(user_id).push().setValue(notification_data).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    Toast.makeText(ProfileActivity.this, "Notification has been saved", Toast.LENGTH_LONG).show();

                                    mCurrent_State = request_sent; // request sent
                                    mProfileSendReqBtn.setText(R.string.cancel_friend_request);

                                    mProfileDecline.setVisibility(View.INVISIBLE);
                                    mProfileDecline.setEnabled(false);
                                }
                            });

                        }
                    });

                }
                else {
                    Toast.makeText(ProfileActivity.this, "Failed Sending request", Toast.LENGTH_SHORT).show();
                }

                mProfileSendReqBtn.setEnabled(true);

            }
        });
    }

}

