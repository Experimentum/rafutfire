package com.example.silver.rafutfirebase;

/**
 * Created by silver on 14.03.18.
 */

public class Post {
    String date;
    String desc;
    String image;
    String time;
    String post_id;
    String title;
    String map;
    String uid;
    String username;
    long creation_date;

    @Override
    public String toString() {
        return "Post{" +
                "date='" + date + '\'' +
                ", desc='" + desc + '\'' +
                ", image='" + image + '\'' +
                ", time='" + time + '\'' +
                ", title='" + title + '\'' +
                ", map='" + map + '\'' +
                ", uid='" + uid + '\'' +
                ", username='" + username + '\'' +
                '}';
    }

    public Post(){

    }



    public Post(String date, String desc, String image, String time, String title, String uid, String username, String map, String post_id, long creation_date) {
        this.date = date;
        this.desc = desc;
        this.image = image;
        this.map = map;
        this.time = time;
        this.title = title;
        this.post_id = post_id;
        this.uid = uid;
        this.username = username;
        this.creation_date = creation_date;
    }

    public long getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(long creation_date) {
        this.creation_date = creation_date;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Post post = (Post) o;

        if (date != null ? !date.equals(post.date) : post.date != null) return false;
        if (desc != null ? !desc.equals(post.desc) : post.desc != null) return false;
        if (image != null ? !image.equals(post.image) : post.image != null) return false;
        if (time != null ? !time.equals(post.time) : post.time != null) return false;
        if (post_id != null ? !post_id.equals(post.post_id) : post.post_id != null) return false;
        if (title != null ? !title.equals(post.title) : post.title != null) return false;
        if (map != null ? !map.equals(post.map) : post.map != null) return false;
        if (uid != null ? !uid.equals(post.uid) : post.uid != null) return false;
        return username != null ? username.equals(post.username) : post.username == null;
    }

    @Override
    public int hashCode() {
        int result = date != null ? date.hashCode() : 0;
        result = 31 * result + (desc != null ? desc.hashCode() : 0);
        result = 31 * result + (image != null ? image.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (post_id != null ? post_id.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (map != null ? map.hashCode() : 0);
        result = 31 * result + (uid != null ? uid.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        return result;
    }
}
