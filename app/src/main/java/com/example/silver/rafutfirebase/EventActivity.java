package com.example.silver.rafutfirebase;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static com.example.silver.rafutfirebase.NonPermanentActivity.REQUEST_LOCATION;


public class EventActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final int GALLERY_REQUEST = 2;
    private Uri uri = null;
    private EditText editName;
    private EditText editDesc;
    private StorageReference storageReference;
    private DatabaseReference databaseReference;
    private DatabaseReference mFriendsPostDatabase;
    private FirebaseUser mCurrentUser;
    private static GoogleMap mMap;
    private static String mapData = null;
    ArrayList markerPoints = new ArrayList();
    private TextView date;
    private static final String TAG = "EventActivity";
    private String selectedDate;
    private TextView time;
    private Switch publicswitch;
    private DatabaseReference selectedDatabase;
    private String username;
    private final int MY_PERMISSIONS_REQUEST = 101;
    private GoogleApiClient googleApiClient;
    private ProgressDialog progressDialog;
    private String formatedTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        editName = findViewById(R.id.editName);
        editDesc = findViewById(R.id.editDesc);
        publicswitch = findViewById(R.id.publicswitch);

        progressDialog = new ProgressDialog(this);


        storageReference = FirebaseStorage.getInstance().getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("rafutfirebase");
        mFriendsPostDatabase = FirebaseDatabase.getInstance().getReference().child("FriendsPosts");

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();
        DatabaseReference mDatabaseUsers = FirebaseDatabase.getInstance().getReference().child("Users").child(mCurrentUser.getUid());
        final LocationManager manager = (LocationManager) EventActivity.this.getSystemService(Context.LOCATION_SERVICE);


        mDatabaseUsers.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                username = dataSnapshot.child("name").getValue().toString();
                Log.d(TAG, "THE USERNAME: " +username);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        date = findViewById(R.id.date);
        time = findViewById(R.id.time);

        check_permissions();

        if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)){

            enableLoc();
        }
    }


    public void check_permissions(){
        if (ContextCompat.checkSelfPermission(EventActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            Log.d(TAG, "Asking for permissions");
            ActivityCompat.requestPermissions(EventActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST);
        }
        else{
            Log.d(TAG, "Permissions have already been granted");

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Log.d(TAG, "Permissons granted");
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.d(TAG, "Permissions denied");
                    Intent intent = new Intent(EventActivity.this, MainMenuActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Please enable GPS to use this feature", Toast.LENGTH_LONG).show();
                    finish();

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private void enableLoc() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(EventActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            googleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {

                            Log.d("Location error", "Location error " + connectionResult.getErrorCode());
                        }
                    }).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(EventActivity.this, REQUEST_LOCATION);

                                //finish();
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                    }
                }
            });
        }
    }



    public static void setMapData(String data) {
        mapData = data;
    }

    //ACTIVITY RESULT FOR IMAGE SELECTION FROM GALLERY
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            Log.d(TAG, "Successfully found an image");
            uri = data.getData();
            ImageButton imageButton = findViewById(R.id.imageButton);
            imageButton.setImageURI(uri);

        }
    }


    // DATE SELECTOR
    public void dateButtonClicked(View view) {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                EventActivity.this, R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear = monthOfYear + 1;

                String monthText = String.valueOf(monthOfYear);
                String dayText = String.valueOf(dayOfMonth);

                if(monthOfYear < 10){monthText = "0"+ monthText;}

                if(dayOfMonth < 10){dayText = "0"+dayText;}

                selectedDate = dayText + "." + monthText + "." + year;

                date.setText(selectedDate);
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    //TIME SELECTOR
    public void timeButtonClicked(View viwe) {
        Calendar cal = Calendar.getInstance();
        int hours = cal.get(Calendar.HOUR);
        int minutes = cal.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(EventActivity.this, R.style.datepicker, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hours, int minute) {
                formatedTime = String.format("%02d:%02d", hours, minute);
                time.setText(formatedTime);

            }
        }, hours, minutes, true);
        timePickerDialog.show();

    }





    //IMAGE BUTTON CLICKED
    public void imageButtonClicked(View view) {
        Intent galleryintent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryintent.setType("image/*");
        Log.d(TAG, "Starting the galary");
        startActivityForResult(galleryintent, GALLERY_REQUEST);
    }


    //SENDING OUR DATA TO THE BACKEND
    public void submitButtonClicked(View view) throws IOException {

        Toast.makeText(EventActivity.this, "Submit button has been clicked", Toast.LENGTH_SHORT).show();

        final String titleValue = editName.getText().toString().trim();
        final String DescValue = editDesc.getText().toString().trim();
        Button submitButton = findViewById(R.id.submitButton);

        Boolean isChecked = publicswitch.isChecked();


            Log.d(TAG, "Public = " + isChecked);

            if(isChecked){

                selectedDatabase = databaseReference;

            }

            else{
                selectedDatabase = mFriendsPostDatabase;
            }

            if (!TextUtils.isEmpty(titleValue) && !TextUtils.isEmpty(DescValue)) {

                progressDialog.setMessage("Uploading post");
                progressDialog.show();
                //save to backend




                StorageReference filePath = storageReference.child("PostImage").child(uri.getLastPathSegment());
                filePath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {


                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        final Uri downloadurl = taskSnapshot.getDownloadUrl();

                        Toast.makeText(EventActivity.this, "Upload complete!", Toast.LENGTH_LONG).show();
                        final DatabaseReference newPost = selectedDatabase.push(); //mFriendsPostDatabase ,databaseReference

                        selectedDatabase.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                String date = new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(new Date());

                                SimpleDateFormat dateFormatGmt = new SimpleDateFormat("ddMMyyyyHHmmss");
                                dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
                                System.out.println("PRINGNGNGN");
                                System.out.println(dateFormatGmt.format(new Date())+"");



                                newPost.child("title").setValue(titleValue);
                                newPost.child("desc").setValue(DescValue);
                                newPost.child("image").setValue(downloadurl.toString());
                                newPost.child("uid").setValue(mCurrentUser.getUid());
                                newPost.child("map").setValue(mapData);
                                newPost.child("creation_date").setValue(Long.valueOf(dateFormatGmt.format(new Date())+"")*-1);
                                newPost.child("post_id").setValue(newPost.getKey());
                                newPost.child("date").setValue(selectedDate);
                                newPost.child("time").setValue(formatedTime);






                                Log.d(TAG, "username"+username);


                                newPost.child("username").setValue(username).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Intent intent = new Intent(EventActivity.this, MainMenuActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                            progressDialog.dismiss();
                                            finish();

                                        }
                                    }
                                });
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


                    }
                });
            }






        submitButton.setEnabled(false);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mMap.setMyLocationEnabled(true);


        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                if (markerPoints.size() > 1) {
                    markerPoints.clear();
                    mMap.clear();
                }

                // Adding new item to the ArrayList
                markerPoints.add(latLng);

                // Creating MarkerOptions
                MarkerOptions options = new MarkerOptions();

                // Setting the position of the marker
                options.position(latLng);

                if (markerPoints.size() == 1) {
                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                } else if (markerPoints.size() == 2) {
                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                }

                // Add new marker to the Google Map Android API V2
                mMap.addMarker(options);

                // Checks, whether start and end locations are captured
                if (markerPoints.size() >= 2) {
                    LatLng origin = (LatLng) markerPoints.get(0);
                    LatLng dest = (LatLng) markerPoints.get(1);

                    // Getting URL to the Google Directions API
                    String url = getDirectionsUrl(origin, dest);

                    DownloadTask task = new DownloadTask();
                    task.execute(url);

                }

            }
        });

    }

    public static void drawMap(PolylineOptions polylineOptions){
        mMap.addPolyline(polylineOptions);
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    public static String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }

        return data;
    }

}
