package com.example.silver.rafutfirebase;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Silver on 28.03.2018.
 */

public class UserRVAdapter extends RecyclerView.Adapter<UsersViewHolder> {

    private List<Users> usersList;

    public UserRVAdapter(){
        this.usersList = new ArrayList<>();
    }

    @NonNull
    @Override
    public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UsersViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.users_single_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final UsersViewHolder holder, int position) {
        holder.setData(usersList.get(position));
        final String user_id = usersList.get(position).getUid();
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profile_intent = new Intent(holder.mView.getContext(), ProfileActivity.class);
                profile_intent.putExtra("user_id", user_id);
                holder.mView.getContext().startActivity(profile_intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public void addAll(List<Users> newUsers){
        int initialSize = usersList.size();


        for (Users user: usersList){
            System.out.println("current list contains : " + user.toString());
        }

        for (Users user : newUsers){
            System.out.println("adding new use : " + user.toString());
        }


        for (Users newuser: newUsers){
            if (!usersList.contains(newuser)){
                usersList.add(newuser);
            }
            else {
                System.out.println("FOUND DUPLIACTTE");
                System.out.println(newuser.toString());
            }
        }

        //usersList.addAll(newUsers);

        notifyItemRangeInserted(initialSize, newUsers.size());
    }

    public String getLastItemId() {
        System.out.println("The size of our userlist is :" + usersList.size());

        for(Users kasutaja : usersList){
            System.out.println(kasutaja.toString());
        }
        return usersList.get(usersList.size() - 1).getUid();
    }
}
