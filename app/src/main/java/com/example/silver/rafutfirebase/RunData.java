package com.example.silver.rafutfirebase;

/**
 * Created by Silver on 11.04.2018.
 */

public class RunData {

    String distance;
    String speed;
    String time;
    String date;
    String dateId;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateId() {
        return dateId;
    }

    public void setDateId(String dateId) {
        this.dateId = dateId;
    }

    public RunData(String distance, String speed, String time, String date, String dateId){
        this.distance = distance;
        this.speed = speed;
        this.time = time;
        this.date = date;
        this.dateId = dateId;


    }
    public RunData(){}

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


}
