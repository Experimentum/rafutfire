package com.example.silver.rafutfirebase;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;

public class FriendsViewActivity extends AppCompatActivity {


    private DatabaseReference mFriendsPostDatabase;
    private static String TAG = "FriendsViewActivity";
    private FirebaseUser mCurrentUser;
    private ArrayList<String> friendsList;


    public class FriendsPostAdapter extends RecyclerView.Adapter<CardVieHolder> {


        ArrayList<Post> items = new ArrayList<>();
        ArrayList<ArrayList<Object>> temp = new ArrayList<ArrayList<Object>>();

        String key = null;

        public FriendsPostAdapter(DatabaseReference ref, final ArrayList<String> friendsList) {
            Log.d(TAG, "The size of our friendslist : " + friendsList.size());


            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    items.clear();
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            ArrayList<Object> singleList = new ArrayList<>();


                            key = postSnapshot.getKey();

                            Post post = postSnapshot.getValue(Post.class);

                            Log.d(TAG, "Gathering data from the database");


                            if (friendsList.contains(post.getUid())) {
                                singleList.add(key);
                                singleList.add(post);

                                temp.add(singleList);

                                items.add(post);

                            }
                        }
                        notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }

        @NonNull
        @Override
        public CardVieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card, parent, false);

            Log.d(TAG, "We are in onCreateViewHolder");
            //         final RecyclerView.ViewHolder viewHolder = new CardVieHolder(view);


//            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent singelPostActivity = new Intent(FriendsViewActivity.this, SingleFriendEventActivity.class);
//                    System.out.println("The key we are forwarding : " + key);
//                    singelPostActivity.putExtra("PostId", key);
//                    startActivity(singelPostActivity);
//
//
//                }
//            });
            return new CardVieHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CardVieHolder holder, int position) {
            Log.d("TAG", "Started binding the data to a view");
            Post item = items.get(position);

            final String post_key = (String) temp.get(position).get(0);


            holder.setUserName(item.getUsername());
            holder.setDesc(item.getDesc());
            holder.setTitle(item.getTitle());
            holder.setImage(getApplicationContext(), item.getImage());
            holder.setTimeAndDate(item.getTime(), item.getDate());

            Log.d("TAG", "Finished binding the data to a view");


            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent singelPostActivity = new Intent(FriendsViewActivity.this, SingleFriendEventActivity.class);
                    singelPostActivity.putExtra("PostId", post_key);
                    startActivity(singelPostActivity);
                    finish();

                }
            });
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_view);


        mFriendsPostDatabase = FirebaseDatabase.getInstance().getReference().child("FriendsPosts");
        DatabaseReference mFriendDatabase = FirebaseDatabase.getInstance().getReference().child("Friends");
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();
        friendsList = new ArrayList<String>();

        mFriendDatabase.child(mCurrentUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String key = snapshot.getKey();
                    Log.d(TAG, "Adding key to friendslist : " + key);
                    friendsList.add(key);
                }
                Log.d(TAG, "Our id : " + mCurrentUser.getUid());
                friendsList.add(mCurrentUser.getUid());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(FriendsViewActivity.this, "Something went wrong when composing the friends list", Toast.LENGTH_SHORT).show();

            }
        });




        Log.d(TAG, "Friends View Activity has been created");
    }


    @Override
    protected void onStart() {
        super.onStart();


        RecyclerView mListItem = findViewById(R.id.list_item);
        mListItem.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        mListItem.setLayoutManager(mLayoutManager);

        Log.d(TAG, "wth");
        FriendsPostAdapter adapter = new FriendsPostAdapter(mFriendsPostDatabase, friendsList);
        mListItem.setAdapter(adapter);

    }

    @Override
    protected void onStop() {
        Log.d(TAG, "Friends View Activity has been stopped");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "Friends View Activity has been destroyed");
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "Friends View Activity has been resumed");
        super.onResume();
    }

    @Override
    protected void onRestart() {
        Log.d(TAG, "Friends View Activity has been restarted");
        super.onRestart();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "Friends View Activity has been paused");
        super.onPause();
    }

    public static class CardVieHolder extends RecyclerView.ViewHolder {

        View mView;

        public CardVieHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setTimeAndDate(String time, String date) {
            TextView post_timeAndDate = mView.findViewById(R.id.timeAndDate);
            String data = "Event starts at " + time + ", on " + date;
            post_timeAndDate.setText(data);
        }


        public void setTitle(String title) {
            TextView post_title = mView.findViewById(R.id.textTitle);
            post_title.setText(title);
        }

        public void setDesc(String desc) {
            TextView post_desc = mView.findViewById(R.id.textDescription);
            System.out.println("this is desc" + desc);
            post_desc.setText(desc);
        }

        public void setImage(Context ctx, String image) {

            ImageView post_image = mView.findViewById(R.id.postImage);
            Picasso.with(ctx).load(image).into(post_image);
        }

        public void setUserName(String userName) {

            TextView postUserName = mView.findViewById(R.id.textUsername);
            String text = "Started by: " + userName;

            postUserName.setText(text);
        }

    }
}
