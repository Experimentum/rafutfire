package com.example.silver.rafutfirebase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class PasswordActivity extends AppCompatActivity {

    FirebaseAuth auth;
    EditText passwordOne;
    EditText passwordTwo;
    Button changeButton;
    Button backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        getSupportActionBar().hide();



        auth = FirebaseAuth.getInstance();
        passwordOne = findViewById(R.id.passFieldOne);
        passwordTwo = findViewById(R.id.passFieldTwo);
        changeButton = findViewById(R.id.submitPwButton);
        backButton = findViewById(R.id.back_button);

    }

    public void onBackButtonClicked(View view){
        Intent intent = new Intent(PasswordActivity.this, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void changePassword(String password){

        FirebaseUser user = auth.getCurrentUser();

        if(user != null){

            user.updatePassword(password).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(PasswordActivity.this, "Password successfuly changed", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(PasswordActivity.this, MainMenuActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            });


        }
    }


    public void changeButtonClicked(View view) {

        if (!passwordOne.getText().toString().isEmpty()) {

            if (passwordOne.getText().toString().equals(passwordTwo.getText().toString())) {
                changePassword(passwordOne.getText().toString());
            } else {
                Toast.makeText(PasswordActivity.this, "The passwords do not match", Toast.LENGTH_LONG).show();
            }
        }
    }
}
