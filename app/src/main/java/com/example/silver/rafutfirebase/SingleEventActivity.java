package com.example.silver.rafutfirebase;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SingleEventActivity extends FragmentActivity implements OnMapReadyCallback {


    private String post_key = null;
    private DatabaseReference mDatabase;
    private TextView singlePostTitle;
    private TextView singlePostDesc;
    private TextView singleDateAndTime;
    private Button deleteButton;
    private FirebaseAuth mAuth;
    private GoogleMap mMap;
    private static String TAG = "SingleEventActivity";
    private String post_map;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_post);

        post_key = getIntent().getExtras().getString("PostId");

        System.out.println("We got, as our intent extra a key called : " + post_key);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("rafutfirebase");

        singlePostDesc = findViewById(R.id.singleDesc);
        singlePostTitle = findViewById(R.id.singleTitle);
        singleDateAndTime = findViewById(R.id.singleDateAndTime);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mAuth = FirebaseAuth.getInstance();
        deleteButton = findViewById(R.id.singleDeleteButton);
        deleteButton.setVisibility(View.INVISIBLE);

        mDatabase.child(post_key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String post_title = (String) dataSnapshot.child("title").getValue();
                String post_desc = (String) dataSnapshot.child("desc").getValue();
                String post_date = (String) dataSnapshot.child("date").getValue();
                String post_time = (String) dataSnapshot.child("time").getValue();

                String dateAndTime = "Event starts at " + post_time + ", on " + post_date;
                String description = "Description: " + post_desc;
                String post_uid = (String) dataSnapshot.child("uid").getValue();
                post_map = (String) dataSnapshot.child("map").getValue();
                DownloadTask task = new DownloadTask();
                task.execute(post_map);

                System.out.println("Post_map" + post_map);

                singlePostTitle.setText(post_title);
                singlePostDesc.setText(description);
                singleDateAndTime.setText(dateAndTime);


                if (mAuth.getCurrentUser().getUid().equals(post_uid)) {
                    deleteButton.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void deleteButtonClicked(View view) {
        mDatabase.child(post_key).removeValue();
        Intent mainIntent = new Intent(SingleEventActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainIntent);
        finish();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mMap.setMyLocationEnabled(true);

    }

    private class DownloadTask extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... url) {
            String data = "";

            try {
                data = url[0];
                System.out.println("THIS IS THE DATA WE ARE WORKING WITH : " + data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();


            parserTask.execute(result);

        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println("THESE ARE OUR ROUTES " + routes);
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points;
            PolylineOptions lineOptions = null;

            if (result == null) {
                return;
            }

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    LatLng position = new LatLng(Double.parseDouble(point.get("lat")), Double.parseDouble(point.get("lng")));

                    Log.v("JSONFIND", position.toString());

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(12);
                lineOptions.color(Color.RED);
                lineOptions.geodesic(true);



            }

            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);

            Log.d(TAG, "Moving the camera to the start point");

            LatLng coordinate = new LatLng(Double.valueOf(result.get(0).get(0).get("lat")), Double.valueOf(result.get(0).get(0).get("lng"))); //Store these lat lng values somewhere. These should be constant.
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(coordinate, 15);
            mMap.animateCamera(location);
        }
    }
}



