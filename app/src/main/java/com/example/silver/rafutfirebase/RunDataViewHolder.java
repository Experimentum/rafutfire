package com.example.silver.rafutfirebase;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Silver on 11.04.2018.
 */

public class RunDataViewHolder extends RecyclerView.ViewHolder {

    TextView timeField;
    TextView distanceField;
    TextView speedField;
    TextView dateField;


    View mView;


    public RunDataViewHolder(View itemView) {
        super(itemView);
        findViews(itemView);
        mView = itemView;
    }


    public void findViews(View view){
        timeField = view.findViewById(R.id.timeField);
        distanceField = view.findViewById(R.id.distanceField);
        speedField = view.findViewById(R.id.speedField);
        dateField = view.findViewById(R.id.dateField);
    }

    public void setData(RunData rundata){

        String timeText = "Time : "+rundata.getTime();
        String distanceText = "Distance : "+rundata.getDistance()+"m";
        String speedText = "Average speed : "+rundata.getSpeed()+"m/s";

        timeField.setText(timeText);
        distanceField.setText(distanceText);
        speedField.setText(speedText);
        dateField.setText(rundata.getDate());



    }
}
