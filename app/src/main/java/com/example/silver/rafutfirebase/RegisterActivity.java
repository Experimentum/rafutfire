package com.example.silver.rafutfirebase;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

public class RegisterActivity extends AppCompatActivity {

    private EditText emailField;
    private EditText passField;
    private EditText secondPassField;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private ProgressDialog progressDialog;
    private static final String TAG = "RegisterActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        passField = findViewById(R.id.passField);
        secondPassField = findViewById(R.id.secondPassField);
        emailField = findViewById(R.id.emailField);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users");

        progressDialog = new ProgressDialog(this);


    }


    public void registerButtonClicked(View view){
        String email = emailField.getText().toString().trim();
        final String pass = passField.getText().toString().trim();
        final String secondPass = secondPassField.getText().toString().trim();


        if(!pass.equals(secondPass)){
            Toast.makeText(this, "Password do not match", Toast.LENGTH_SHORT).show();
            //stopping the function
            return;
        }


        if(TextUtils.isEmpty(email)){
            //email is empty
            Toast.makeText(this, "Please enter an email.", Toast.LENGTH_SHORT).show();
            //stopping the function
            return;
        }
        if(TextUtils.isEmpty(pass)){
            //password is empty
            Toast.makeText(this, "Please enter a password.", Toast.LENGTH_SHORT).show();
            //stopping the function
            return;
        }


        //if valid
        progressDialog.setMessage("Registration in progress");
        progressDialog.show();


        if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(pass) && !TextUtils.isEmpty(secondPass)){
            mAuth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()){
                        finish();
                        String user_id = mAuth.getCurrentUser().getUid();

                        String device_token = FirebaseInstanceId.getInstance().getToken();

                        DatabaseReference current_user_db = mDatabase.child(user_id);
                        current_user_db.child("image").setValue("default");
                        current_user_db.child("device_token").setValue(device_token);

                        Log.d(TAG, "SENDING TO SETUP");
                        Intent mainIntent = new Intent(RegisterActivity.this, SetupActivity.class);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(mainIntent);
                        finish();
                    }
                    if(!task.isSuccessful()){
                        try {
                            throw task.getException();}

                        catch(FirebaseAuthWeakPasswordException e) {
                            progressDialog.dismiss();
                            passField.setError("Password is too weak");
                            passField.requestFocus();
                        }
                        catch(FirebaseAuthInvalidCredentialsException e) {
                            progressDialog.dismiss();
                            emailField.setError("Invalid email");
                            emailField.requestFocus();
                        }
                        catch(FirebaseAuthUserCollisionException e) {
                            progressDialog.dismiss();
                            emailField.setError("User already exists");
                            emailField.requestFocus();
                        }
                        catch(Exception e) {
                            Log.e(TAG, e.getMessage());
                        }
                    }

                }
            });
        }
    }

}
