package com.example.silver.rafutfirebase;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class NonPermanentActivity extends FragmentActivity implements OnMapReadyCallback {

    public final static double AVERAGE_RADIUS_OF_EARTH_METERS = 6371000;
    final static int REQUEST_LOCATION = 199;
    private final String TAG = "Permanent";
    private final int MY_PERMISSIONS_REQUEST = 101;
    Double latitude, longitude;
    LatLng currentLocation = new LatLng(0, 0);
    Button start_button;
    Button stop_button;
    TextView avg_speed;
    TextView distanceView;
    double totalDistance;
    String date;

    List<Polyline> polylines;
    double speed;
    ValueEventListener listener = null;
    private GoogleMap mMap;
    private Chronometer mChronometer;
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            //Toast.makeText(NonPermanentActivity.this, "Broadcast received", Toast.LENGTH_SHORT).show();

            latitude = Double.valueOf(intent.getStringExtra("latutide"));
            longitude = Double.valueOf(intent.getStringExtra("longitude"));

            Log.d(TAG, "latitude" + latitude);
            Log.d(TAG, "longitude" + longitude);

            LatLng myCoordinates = new LatLng(latitude, longitude);



            if (currentLocation.latitude == 0 && currentLocation.longitude == 0) {
                currentLocation = myCoordinates;
                return;
            } else {
                Log.d(TAG, "Our current coordinates are: " + myCoordinates);
                Log.d(TAG, "Our old coordinates are: " + currentLocation);



                double distance = calculateDistanceInMeters(currentLocation.latitude, currentLocation.longitude, myCoordinates.latitude, myCoordinates.longitude);

                totalDistance += distance;

                Log.d(TAG, "THE DISTANCE : " + String.valueOf(distance));

                String stringTotalDistance = String.valueOf(totalDistance);

                distanceView.setText(stringTotalDistance);

                double time = (double) (SystemClock.elapsedRealtime() - mChronometer.getBase());

                double seconds = time / 1000;

                Log.d(TAG, "THE TIME : " + String.valueOf(seconds));


                //Toast.makeText(NonPermanentActivity.this, String.valueOf(seconds), Toast.LENGTH_SHORT).show();


                speed = Math.round(totalDistance / seconds * 100.0) / 100.0;

                String stringSpeed = String.valueOf(speed);

                avg_speed.setText(stringSpeed);


                Polyline line = mMap.addPolyline(new PolylineOptions()
                        .add(currentLocation, myCoordinates)
                        .width(12)
                        .color(Color.RED).geodesic(true));


                polylines.add(line);



                Log.d(TAG, "Line has been drawn");
            }



            //saving my current location to the global variable
            currentLocation = myCoordinates;

            mMap.moveCamera(CameraUpdateFactory.newLatLng(myCoordinates));


        }

        ;
    };
    private long lastPause;
    private DatabaseReference mActivityDatabase;
    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_non_permanent);

        Toast.makeText(NonPermanentActivity.this, "started", Toast.LENGTH_SHORT).show();
        //startService(new Intent(NonPermanentActivity.this, LocationService.class));
        final LocationManager manager = (LocationManager) NonPermanentActivity.this.getSystemService(Context.LOCATION_SERVICE);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        start_button = findViewById(R.id.start_button);
        stop_button = findViewById(R.id.stop_button);
        polylines = new ArrayList<Polyline>();

        mChronometer = findViewById(R.id.chronometer);
        avg_speed = findViewById(R.id.avg_speed);
        distanceView = findViewById(R.id.distance);

        check_permissions();

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            enableLoc();
        }


    }

    public void check_permissions() {
        if (ContextCompat.checkSelfPermission(NonPermanentActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            Log.d(TAG, "Asking for permissions");
            ActivityCompat.requestPermissions(NonPermanentActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST);
        } else {
            Log.d(TAG, "Permissions have already been granted");

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Log.d(TAG, "Permissons granted");
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.d(TAG, "Permissions denied");
                    Intent intent = new Intent(NonPermanentActivity.this, MainMenuActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Please enable GPS to use this feature", Toast.LENGTH_LONG).show();
                    finish();

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private void enableLoc() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(NonPermanentActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            googleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {

                            Log.d("Location error", "Location error " + connectionResult.getErrorCode());
                        }
                    }).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(NonPermanentActivity.this, REQUEST_LOCATION);

                                //finish();
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                    }
                }
            });
        }
    }

    private void processStartService(final String tag) {
        Intent intent = new Intent(getApplicationContext(), LocationService.class);
        intent.addCategory(tag);
        startService(intent);
    }

    private void processStopService(final String tag) {
        Intent intent = new Intent(getApplicationContext(), LocationService.class);
        intent.addCategory(tag);
        stopService(intent);
    }

    public void startButtonClicked(View view) {
        // TODO: 10.04.2018
        Toast.makeText(NonPermanentActivity.this, "Start button has been clicked", Toast.LENGTH_SHORT).show();
        // startReceiver();


        if (lastPause != 0) {
            mChronometer.setBase(mChronometer.getBase() + SystemClock.elapsedRealtime() - lastPause);
        } else {
            mChronometer.setBase(SystemClock.elapsedRealtime());
        }

        mChronometer.start();


        processStartService(LocationService.TAG);

        start_button.setEnabled(false);
        stop_button.setEnabled(true);
        totalDistance = 0;


    }

    public void stopButtonClicked(View view) {
        // TODO: 10.04.2018
        Toast.makeText(NonPermanentActivity.this, "Stop button has been clicked", Toast.LENGTH_SHORT).show();
        // startReceiver();

        processStopService(LocationService.TAG);

        start_button.setEnabled(true);
        stop_button.setEnabled(false);

        sendToDatabase();
        mChronometer.stop();
        mChronometer.setBase(SystemClock.elapsedRealtime());

        avg_speed.setText("0.00");
        distanceView.setText("0.00");

        clearmMap();
    }

    public void cancelButtonClicked(View view) {
        processStopService(LocationService.TAG);

        start_button.setEnabled(true);
        stop_button.setEnabled(true);

        mChronometer.stop();
        mChronometer.setBase(SystemClock.elapsedRealtime());

        avg_speed.setText("0.00");
        distanceView.setText("0.00");

        clearmMap();
    }

    private void deActivateConnection() {
        mActivityDatabase.removeEventListener(listener);
    }


    //IntentFilter filter = new IntentFilter("message"); KUI PUTSI LÄHEB KOMMENTEERI TAGASI SISSE

    private void sendToDatabase() {
        FirebaseUser mCurrent_user = FirebaseAuth.getInstance().getCurrentUser();
        mActivityDatabase = FirebaseDatabase.getInstance().getReference().child("ActivityDatabase").child(mCurrent_user.getUid());

        final DatabaseReference newPost = mActivityDatabase.push();

        String elapsedTime = mChronometer.getText().toString();


        date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());

        String dateId = new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(new Date());


        mActivityDatabase.addValueEventListener(listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                newPost.child("time").setValue(elapsedTime);
                newPost.child("distance").setValue(String.valueOf(totalDistance));
                newPost.child("date").setValue(String.valueOf(date));
                //newPost.child("dateId").setValue(dateId);
                newPost.child("speed").setValue(String.valueOf(speed)).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        deActivateConnection();
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "Error : " + databaseError);
            }
        });

        //


    }

    public void clearmMap() {

        if (polylines.size() > 0) {
            //Removing all polylines from the map
            for (Polyline line : polylines) {
                line.remove();
            }
            polylines.clear();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter("message"));
    }

    protected void onPause() {
        super.onPause();
        Log.d(TAG, "Location services have been paused");
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

    }

    public int calculateDistanceInMeters(double userLat, double userLng,
                                         double venueLat, double venueLng) {

        double latDistance = Math.toRadians(userLat - venueLat);
        double lngDistance = Math.toRadians(userLng - venueLng);

        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(venueLat))
                * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return (int) (Math.round(AVERAGE_RADIUS_OF_EARTH_METERS * c));
    }
}
