package com.example.silver.rafutfirebase;

/**
 * Created by silver on 30.01.18.
 */

public class Cards {

    private static String TAG = "Cards";
    private String title;
    private String desc;
    private String image;
    private String time;
    private String date;
    private String username;


    public Cards(){

    }

    public Cards(String title, String desc, String image, String username, String time, String date){
        this.title = title;
        this.desc = desc;
        this.image = image;
        this.username = username;
        this.time = time;
        this.date = date;
    }

    public String getDate() {
        System.out.println("This is the day in the cards: " +date);
        return date;
    }

    public String getTime() {
        return time;
    }

    public void setDate(String day) {
        this.date = day;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


//    public String getMap() {
//        return map;
//    }
//
//    public void setMap(String map) {
//        this.map = map;
//    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public String getImage() {
        return image;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
