package com.example.silver.rafutfirebase;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Silver on 04.04.2018.
 */

public class PublicPostsAdapter extends RecyclerView.Adapter<CardVieHolder> {

    private List<Post> postsList;
    private static String TAG = "PublicPostsAdapter";


    public PublicPostsAdapter() {
        this.postsList = new ArrayList<>();
    }

    @NonNull
    @Override
    public CardVieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CardVieHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.card, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CardVieHolder holder, int position) {

        String post_key = postsList.get(position).getPost_id();

        holder.setTitle(postsList.get(position).getTitle());
        holder.setDesc(postsList.get(position).getDesc());
        holder.setUserName(postsList.get(position).getUsername());
        holder.setTimeAndDate(postsList.get(position).getTime(), postsList.get(position).getDate());
        holder.setImage(holder.mView.getContext(), postsList.get(position).getImage()); // WE MIGHT GET PROBLEMS FROM HERE MIGHT

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent singelPostActivity = new Intent(holder.mView.getContext(), SingleEventActivity.class);
                Log.d(TAG, "The post key we are sending with out intent : " + post_key);
                singelPostActivity.putExtra("PostId", post_key);
                holder.mView.getContext().startActivity(singelPostActivity);

            }
        });


    }

    @Override
    public int getItemCount() {
        return postsList.size();
    }

    public void addAll(List<Post> newPosts) {
        int initialSize = postsList.size();

        System.out.println("Postlist size : " + initialSize);

        for (Post newPost : newPosts) {
            if (!postsList.contains(newPost)) {

                System.out.println("adding new post : " + newPost);

                postsList.add(newPost);
            } else {
                System.out.println("FOUND DUPLIACTTE");
                System.out.println(newPost.toString());
            }
        }

        notifyItemRangeInserted(initialSize, newPosts.size());

    }

    public long getLastItemId() {

        for (Post post : postsList) {
            System.out.println("our posts : " + post.getPost_id());
        }


        return postsList.get(postsList.size() - 1).getCreation_date();
    }
}
