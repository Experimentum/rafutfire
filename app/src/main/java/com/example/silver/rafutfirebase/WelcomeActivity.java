package com.example.silver.rafutfirebase;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class WelcomeActivity extends AppCompatActivity {

    public static String TAG = "WelcomeActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);



        Button login = (Button) findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();

            }
        });

        Button register = (Button) findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(WelcomeActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "Welcome activity has been destroyed");
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "Welcome activity has been stopped");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        Log.d(TAG, "Welcome activity has been restarted");
        super.onRestart();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "Welcome activity has been resumed");
        super.onResume();
    }
}
