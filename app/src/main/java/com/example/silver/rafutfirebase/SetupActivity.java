package com.example.silver.rafutfirebase;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class SetupActivity extends AppCompatActivity {

    private EditText editDisplayName;
    private CircleImageView displayImage;
    private static final int GALLERY_REQ = 1;
    private Uri mImageUri = null;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseusers;
    private StorageReference mStorageref;
    private EditText setupStatusField;
    private ProgressDialog progressDialog;
    private EditText setupLocationField;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        getSupportActionBar().hide();

        editDisplayName = findViewById(R.id.displayName);
        displayImage = findViewById(R.id.setupImageButton);
        setupStatusField = findViewById(R.id.setupStatus);
        progressDialog = new ProgressDialog(this);
        Button done_button = findViewById(R.id.dataChangeDone);

        mDatabaseusers = FirebaseDatabase.getInstance().getReference().child("Users");
        mAuth = FirebaseAuth.getInstance();
        mStorageref = FirebaseStorage.getInstance().getReference().child("profile_image");
        setupLocationField = findViewById(R.id.setupLocation);


    }

    public void profileImageButtonClicked(View view) {
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, GALLERY_REQ);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQ && resultCode == RESULT_OK) {

            Uri imageUri = data.getData();
            CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(this);

        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mImageUri = result.getUri();
                displayImage.setImageURI(mImageUri);
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }


    }


    public void doneButtonClicked(View view) {

        Log.d("SetupActivity", "donebuttonclicked");

        final String name = editDisplayName.getText().toString().trim();
        final String user_id = mAuth.getCurrentUser().getUid();
        final String status = setupStatusField.getText().toString().trim();
        final String location = setupLocationField.getText().toString().trim();
        if (!TextUtils.isEmpty(name) && mImageUri != null) {


            File filePath = new File(mImageUri.getPath());

            Bitmap thumb_file = null;

            try {
                thumb_file = new Compressor(this).setMaxWidth(200)
                        .setMaxHeight(200)
                        .setQuality(75)
                        .compressToBitmap(filePath);
            } catch (IOException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            thumb_file.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            final byte[] thumb_byte = baos.toByteArray();

            final StorageReference thumb_file_path = mStorageref.child("profile_image").child("thumbs").child(user_id + ".jpg");

            StorageReference filepath = mStorageref.child(mImageUri.getLastPathSegment());


            progressDialog.setMessage("Saving your settings");
            progressDialog.show();
            filepath.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    final String downloadurl = String.valueOf(taskSnapshot.getDownloadUrl());

                    UploadTask uploadTask = thumb_file_path.putBytes(thumb_byte);
                    uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> thumb_task) {

                            mDatabaseusers.child(user_id).child("name").setValue(name);
                            mDatabaseusers.child(user_id).child("status").setValue(status);
                            mDatabaseusers.child(user_id).child("uid").setValue(user_id);
                            mDatabaseusers.child(user_id).child("location").setValue(location);

                            String thumb_downloadURL = thumb_task.getResult().getDownloadUrl().toString();

                            if (thumb_task.isSuccessful()) {

                                Map<String, Object> update_hasmap = new HashMap<>();
                                update_hasmap.put("image", downloadurl);
                                update_hasmap.put("thumb_image", thumb_downloadURL);

                                mDatabaseusers.child(user_id).updateChildren(update_hasmap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        progressDialog.dismiss();
                                        Intent intent = new Intent(SetupActivity.this, MainMenuActivity.class);
                                        startActivity(intent);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        finish();
                                    }
                                });


                            }

                        }
                    });


                }


            });


        }

    }
}
